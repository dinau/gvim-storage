#!/bin/bash
DEBUG=1
if test $DEBUG -eq 0  ; then
   CMD=
else
   CMD="echo"
fi
tbl_file=(
'.vimrc'
'.gvimrc'
'.vim/autoload/plug.vim'
'.vim/colors/dos-color.vim'
'.vim/indent/cmake-indent.vim'
'.vim/syntax/arduino.vim'
'.vim/syntax/armasm.vim'
'.vim/syntax/cmake-syntax.vim'
'.vim/syntax/delphi.vim'
'.vim/syntax/forth.vim'
'.vim/syntax/jal.vim'
'.vim/syntax/mips.vim'
'.vim/syntax/nim.vim'
'.tigrc'
'.gitconfig'
'.gitignore'
)
tbl_dir=(
'.vim'
'.vim/autoload'
'.vim/colors'
'.vim/indent'
'.vim/syntax'
'.vim/vimbackup'
)

for (( i=0; i < ${#tbl_dir[@]}; ++i ))
do
    dirname=~/${tbl_dir[$i]}
    if [ ! -d $dirname ] ; then
        $CMD mkdir -p $dirname
    fi
done

BAK=~/bak
if [ ! -d $BAK ] ; then
    mkdir $BAK
fi

for (( i=0; i < ${#tbl_file[@]}; ++i ))
do
    fname=${tbl_file[$i]}
    # move original to ~/bak
    if [ -f ~/$fname ]; then
        # del link
        if [ -L ~/$fname ]; then
            $CMD unlink ~/$fname
        else
            $CMD mv ~/$fname $BAK/
        fi
    fi
    $CMD ln -s `pwd`/$fname ~/$fname
done

