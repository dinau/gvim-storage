"http://ac-mopp.blogspot.com/2014/02/vimrc.html
"-----------------------------------------------------------------------------------"
" Mappings                                                                          |
"-----------------------------------------------------------------------------------"
" コマンド        | ノーマル | 挿入 | コマンドライン | ビジュアル | 選択 | 演算待ち |
" map  / noremap  |    @     |  -   |       -        |     @      |  @   |    @     |
" nmap / nnoremap |    @     |  -   |       -        |     -      |  -   |    -     |
" vmap / vnoremap |    -     |  -   |       -        |     @      |  @   |    -     |
" omap / onoremap |    -     |  -   |       -        |     -      |  -   |    @     |
" xmap / xnoremap |    -     |  -   |       -        |     @      |  -   |    -     |
" smap / snoremap |    -     |  -   |       -        |     -      |  @   |    -     |
" map! / noremap! |    -     |  @   |       @        |     -      |  -   |    -     |
" imap / inoremap |    -     |  @   |       -        |     -      |  -   |    -     |
" cmap / cnoremap |    -     |  -   |       @        |     -      |  -   |    -     |
"-----------------------------------------------------------------------------------"
"""""""""""""""""""""""""""""""""""""""""""""""""
" .vimrc file
" Change history:
"	2015/06:
"		Maintainer:	audin <http://mpu.seesaa.net>
"	2012/06:
"		Maintainer:	audin <http://avr.paslog.jp>
"------------------------------------------------
" os = win32, unix, win32unix
"------------------------------------------------
"--- 読み込み時に試す順。エンコーディングの設定。
if has('win32') || has('win64')
    set encoding=utf-8
endif
"set fileencodings=iso-2022-jp,euc-jp,sjis,utf-8
"set fileencodings=cp932,utf-8,euc-jp,utf-16le,utf-16
set fileformats=dos,unix,mac

"--- 無限アンドゥの許可の可否。
"set undofile
set undodir=d:/00emacs-home/vimundo

"---  unite.vim に関連するプラグインを使う(=1)か使わないか。
	let g:unite = 0

"---------------plug---------------start
if has('win32') || has('win64')
    call plug#begin('~/vimfiles/plugged')
else
    call plug#begin('~/.vim/plugged')
endif

"--- 必須プラグイン plug-in start
    Plug 'vim-jp/vimdoc-ja'
    Plug 'sandeepcr529/Buffet.vim'
    Plug 'dinau/mru.vim'
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-repeat'
    "Plug 'vim-scripts/YankRing.vim'
    Plug 'nathanaelkane/vim-indent-guides'
    Plug 'zah/nim.vim'
"--- 必須プラグイン end

Plug 'yegappan/grep'
"Plug 'Shougo/neomru.vim'
"--- deoplete
        Plug 'Shougo/deoplete.nvim'
        Plug 'roxma/nvim-yarp'
        Plug 'roxma/vim-hug-neovim-rpc'
        Plug 'lighttiger2505/deoplete-vim-lsp'
        let g:deoplete#enable_at_startup = 1
          "let g:deoplete#auto_complete_delay = 0
        "call deoplete#custom#option({
        "     \   'auto_complete_delay': 0,
        "     \   'smart_case': v:false,
        "     \   'auto_complete_start_length':  1,
        "     \   'ignore_case': v:true,
        "     \   'refresh_always': v:false,
        "     \   'file#enable_buffer_path': 1,
        "     \   'max_list': 10000
        "     \ })
          "  "入力からポップアップ表示までのdealy[ms]
        "大文字小文字を区別
              "https://www-tap.scphys.kyoto-u.ac.jp/~shima/neovim.php
"--- end deoplete
"
Plug 'sukima/xmledit'
Plug 'vim-scripts/vcscommand.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-scripts/taglist.vim'
Plug 'kana/vim-fakeclip'
"Plug 'Lokaltog/vim-easymotion'
"Plug 'scrooloose/nerdtree'
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'junegunn/gv.vim'
let g:loaded_matchparen = 1
"Plug 'itchyny/vim-parenmatch'
"Plug 'itchyny/vim-cursorword'
Plug 'hallison/vim-markdown'
"空白削除
"Plug 'bronson/vim-trailing-whitespace'

"Plug 'itchyny/lightline.vim'
"Plug 'h1mesuke/unite-outline'
Plug 'vim-scripts/Align'
"--- mark down
"Plug 'https://github.com/Shougo/quicrun.vim.git'
Plug 'kannokanno/previm'
"Plug 'thinca/vim-quickrun'
Plug 'tyru/open-browser.vim'
Plug 'superbrothers/vim-quickrun-markdown-gfm'

Plug 'rhysd/vim-gfm-syntax'

"let g:syntastic_en=0
"if has('nvim'
"    let g:syntastic_en=0
"endif
"if g:syntastic_en==1
"    Plug 'scrooloose/syntastic'
"endif
Plug 'tpope/vim-endwise'
"Plug 'thinca/vim-ref'
"Plug 'yuku-t/vim-ref-ri'
Plug 'vim-jp/vim-go-extra'
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

Plug 'timonv/vim-cargo'
"Plug 'chrisbra/csv.vim'
"Plug 'cespare/vim-toml.vim'

"---start vim-gitgutter
Plug 'airblade/vim-gitgutter'
if has('win32') || has('win64')
    let g:gitgutter_git_executable = 'd:\Git\bin\git.exe'
else
    let g:gitgutter_git_executable = '/usr/bin/git'
endif
set updatetime=100
let g:gitgutter_override_sign_column_highlight = 0
highlight SignColumn ctermbg=brown
let g:gitgutter_sign_added = '*A'
let g:gitgutter_sign_modified = '*M'
let g:gitgutter_sign_removed = '--'
let g:gitgutter_sign_removed_first_line = '-1'
let g:gitgutter_sign_modified_removed = '-M'
"---end  vim-gitgutter
Plug 'mattn/emmet-vim'
"Plug 'dense-analysis/ale'
Plug 'luochen1990/rainbow'
" RainbowToggle
let g:rainbow_active = 0
" Clang
Plug 'justmao945/vim-clang'
" nimlsp
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete-lsp.vim'

call plug#end()
"---------------plug---------------end

if has('nvim')
"    call asyncomplete#register_source({
"        \ 'name': 'nim',
"        \ 'whitelist': ['nim'],
"        \ 'completor': {opt, ctx -> nim#suggest#sug#GetAllCandidates({start, candidates -> asyncomplete#complete(opt['name'], ctx, start, candidates)})}
"        \ })
endif


"------ プラグインの読み込み
runtime $VIM/macros/matchit.vim
"--- netrw
" 上部に表示される情報を非表示
let g:netrw_banner = 0
" 表示形式をTreeViewに変更
let g:netrw_liststyle = 1
"-- Helpを起動時から日本語表示
set helplang=ja,en

"----- deoplete-vim-lsp
" For python language server
if (executable('pyls'))
    let s:pyls_path = fnamemodify(g:python_host_prog, ':h') . '/'. 'pyls'
    augroup LspPython
        autocmd!
        autocmd User lsp_setup call lsp#register_server({
      \ 'name': 'pyls',
      \ 'cmd': {server_info->['pyls']},
      \ 'allowlist': ['python']
      \ })
    augroup END
endif
"------ nimlsp
let s:nimlspexecutable = "nimlsp"
let g:lsp_log_verbose = 1
let g:lsp_log_file = expand('/tmp/vim-lsp.log')
" for asyncomplete.vim log
let g:asyncomplete_log_file = expand('/tmp/asyncomplete.log')
let g:asyncomplete_auto_popup = 1
if has('win32')
   let s:nimlspexecutable = "nimlsp.cmd"
   " Windows has no /tmp directory, but has $TEMP environment variable
   let g:lsp_log_file = expand('$TEMP/vim-lsp.log')
   let g:asyncomplete_log_file = expand('$TEMP/asyncomplete.log')
endif
if executable(s:nimlspexecutable)
   au User lsp_setup call lsp#register_server({
   \ 'name': 'nimlsp',
   \ 'cmd': {server_info->[s:nimlspexecutable]},
   \ 'whitelist': ['nim'],
   \ })
endif
function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction
inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ asyncomplete#force_refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

"------ Clang言語用
"------ clangd
let g:lsp_log_verbose = 1
let g:lsp_log_file = expand('~/vim-lsp.log')
call lsp#register_server({
    \ 'name': 'clangd',
    \ 'cmd': {server_info->['clangd']},
    \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
    \ })

" オムニ補完設定
" autocmd FileType typescript setlocal omnifunc=lsp#complete

"-------------
let g:clang_c_options = '-std=c11'
let g:clang_cpp_options = '-sdt=c++1z -stdlib=libc++ -pedantic-errors'
let g:clang_format_auto = 1
let g:clang_format_style = 'Google'
let g:clang_check_syntax_auto = 1
"------ Rust言語用
"--- rustfmt
"let g:rustfmt_autosave = 1
let g:rustfmt_command = 'c:/Users/mi/.cargo/bin/rustfmt.exe'
let g:rustfmt_options=' --force '
let g:rustfmt_fail_silently = 1
"--- racer
let g:racer_cmd = 'c:/Users/mi/.cargo/bin/racer.exe'
let g:racer_experimental_completer = 1
"let $RUST_SRC_PATH=''
au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap <leader>gd <Plug>(rust-doc)
"------ Go言語用
"--  https://mattn.kaoriya.net/software/vim/20130531000559.htm
exe "set rtp+=".globpath($GOPATH, "src/github.com/nsf/gocode/vim")
set completeopt=menu,preview
"exe "set rtp+=".globpath($GROOT, "bin")

"------ 絶対パスが必要な設定
"--- アックアップ用のディレクトリ 必須
set writebackup

if has('win32') || has('win64')
    set backupdir=$HOME/vimfiles/vimbackup
    set directory=$HOME/vimfiles/vimbackup
    set undodir=$HOME/vimfiles/vimbackup
else
    set backupdir=$HOME/.vim/vimbackup
    set directory=$HOME/.vim/vimbackup
    set undodir=$HOME/.vim/vimbackup
endif

" 行末の空白(space)を保存時に自動削除
"http://qiita.com/mktakuya/items/2a6cd35ca0c1b217e28c
"autocmd BufWritePre ^"*.md" :%s/\s\+$//ge
" 但し、markdownファイルは除く
"https://stackoverflow.com/questions/6496778/vim-run-autocmd-on-all-filetypes-except
fun! StripTrailingWhitespace()
    " Don't strip on these filetypes
    "if &ft =~ 'ruby\|javascript\|perl'
    if &ft =~ 'markdown'
        return
    endif
    %s/\s\+$//ge
endfun
autocmd BufWritePre * call StripTrailingWhitespace()


" :CdCurrent
"   Change current directory to current file's one.
command! -nargs=0 CdCurrent cd %:p:h
nnoremap <Space>d :CdCurrent<CR>

"------ プラグイン(plugin)のオプション
"--- indent guides settings
if has('gui_running')
	let g:indent_guides_enable_on_vim_startup = 1
	let g:indent_guides_auto_colors = 0
	autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#101010 ctermbg=3
	autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#000000 ctermbg=4
endif

"------ 便利キー設定 Key settings
" https://liginc.co.jp/409849
" ノーマルモード移行と保存
"inoremap <silent>jj <ESC>:<C-u>w<CR>
inoremap <silent>jj <ESC>
"nnoremap kk :
nnoremap <Space>r :RustRun<CR>
nnoremap <Space>e :e!<CR>
"--- .vimrcを開く。専用ホットキーを定義する場合
nnoremap <Space>.  :<C-u>edit $MYVIMRC<CR>
"--- source ~/.vimrc を実行する。
nnoremap <Space>,  :<C-u>source $MYVIMRC<CR>
"--- 行ラップ切り替え
nnoremap <Leader>w  :set wrap!<CR>
"--- ファイラ、バッファリスト、ファイルオープン履歴
nnoremap <silent><Space>j	:Explore<CR>
nnoremap <silent><Space>l	:Bufferlist<CR>
nnoremap <silent><Space>m	:MRU<CR>
"----- mru.vim
"https://sites.google.com/site/fudist/Home/modify
let MRU_Auto_Close = 1
let MRU_Use_Alt_useopen = 1
" カーソルラインを使用する
let MRU_Use_CursorLine = 1
"let MRU_Window_Height = 20
let MRU_Max_Entries   = 200
"""""""""""""""""""""""""""""""""""""""""""""""""
" Referred to : http://vimwiki.net/?tips%2F118
" Vim 標準装備のMRUを緊急時に使う時
" :ol       MRUリストを出す
" :e #<N	Nにファイル番号でオープン
"--- Vim 標準装備のMRUを出す
"nnoremap <silent><C-m>		:ol<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""
"--- テキストの保存
"nnoremap <silent> <Space>s :<C-u>update<CR>
nnoremap <silent> <Space>s :w<CR>
"--- 次のバッファ、前のバッファ、バッファの削除、バッファのリスト
nnoremap <silent><Space>n	:bn<CR>
nnoremap <silent><Space>b	:bp<CR>
nnoremap <silent><Space>k	:bd<CR>
"nnoremap <silent><Space>k	:close<CR>
"--- カーソルのあるウインドウを最大化する
nnoremap <Space>o	<C-W>o
"---  カーソルのあるウインドウを隠す
nnoremap <silent><Space>h	:hide<CR>
"--- 最後に変更されたテキストを選択する
"nnoremap gc  `[v`]
"  http://qiita.com/us10096698/items/e1ec495fb8682c906a81
vmap ,y "*y
nmap ,p "*p
"---  yank ring
"nnoremap <silent> <Space>y :YRShow<CR>
nnoremap <silent> <Space>y :reg<CR>
" let g:yankring_replace_n_pkey = 'P'
" let g:yankring_replace_n_nkey = 'N'
" :make の出力を実行中に見たい
"set shellpipe=\|\ tee

"--- omni 補完
imap <Space>w <C-x><C-o>
"--- tags 設定
set tags=./tags;,tags;
"------ Gtags
"--- 現在のファイルの関数一覧を表示
nnoremap <Space>f	:CdCurrent<CR>:Gtags -f %<CR>
" カーソル上の単語を Gtagsする
nnoremap <Space>g	:CdCurrent<CR>:GtagsCursor<CR>
" Gtagsをアップデート
nnoremap <Space>u	:CdCurrent<CR>:GtagsUpdate<CR>
"--- 次の検索結果へジャンプする tag
nnoremap <C-n> :cn<CR>
"nnoremap <Space>] :cn<CR>
"--- 前の検索結果へジャンプする tag
"nnoremap <Space>[ :cp<CR>
nnoremap <C-p> :cp<CR>
" 開いているファイルに定義されている関数の一覧を表示
"nnoremap <C-i> :Gtags -f %<CR>
"関数から戻る
"nnoremap <Space>x <C-o>
" Change directory
nnoremap <Space>c	:Cda<CR>
"------ Tab selsect
"--- Next tab
nnoremap <S-TAB>	:tabn<CR>
"--- Prev tab
nnoremap <C-TAB>	gT<CR>

" vcscommand の割り当て
" noremap <Space>d	:VCSVimDiff<CR>
"--- vimshell
noremap <Space>v	:VimShellPop %:p:h<CR>
nnoremap <silent> ,ug :Unite grep:%:-iHRn<CR>
let g:vimshell_interactive_update_time = 10

"--- taglist
let Tlist_Show_One_File = 1
"let Tlist_Use_Right_Window = 1
let Tlist_Exit_OnlyWindow = 1
noremap <Leader>f :TlistToggle<CR>
noremap <Leader>tr :TrinityToggleAll<CR>

"--- markdown preview key
noremap <Leader>m :PrevimOpen<CR>
"--- http://sites.google.com/site/fudist/Home/vim-nihongo-ban/tips#TOC-9
"ciyはカーソル位置の単語をヤンクした文字列に置き換えます。
"ciyはテキストオブジェクトなのでカーソルが単語内の何処にあっても使えます。
"cyはカーソル位置以降の単語部分をヤンクした単語に置換します。
"一度cyやciyを実行した後、n.で繰り返せる用に、検索レジスタが置換された文字列と置き換わります。
nnoremap <silent> ciy ciw<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
nnoremap <silent> cy   ce<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
vnoremap <silent> cy   c<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
"    置換に使いたい文字列をヤンクしておきます。
"    置換したい単語まで移動します。
"    cyかciyを実行します。
"    カーソル位置の単語がヤンクした文字列に置換されます。
"    必要ならn.で繰り返します。
"同様にヤンクした文字を貼り付けます。
nnoremap gy "0P
"この場合はカーソル位置の前に挿入したいことが多いので、Pで貼り付けています。
"単純にpだとレジスタの内容が入れ替わっていることも多いのでヤンクレジスタを指定して貼り付けます。

"--- makeの実行
nnoremap <Space>/ :make<CR><CR>
"nnoremap <silent><Space>/ :CdCurrent<CR>make<CR><CR>
"--- Nerd_Commenter の基本設定
" refer to http://nishikawasasaki.hatenablog.com/entry/20101226/1293374432
let g:NERDCreateDefaultMappings = 1
let NERDSpaceDelims = 1
vmap <Leader>; <Plug>NERDCommenterToggle
nmap <Leader>; <Plug>NERDCommenterToggle
nmap <Leader>;a <Plug>NERDCommenterAppend
nmap <leader>;9 <Plug>NERDCommenterToEOL
vmap <Leader>;s <Plug>NERDCommenterSexy
vmap <Leader>;b <Plug>NERDCommenterMinimal
"--- Cygwin only: カーソルキーがおかしい対応
if has("win32unix")
set mouse=a
"if has("win32unix")
	" For Arrow keys
"set ttimeoutlen=100
"	set term=ansi
	set t_ku=OA
	set t_kd=OB
	set t_kl=OD
	set t_kr=OC
"	set t_ku="\eOA"
"	set t_kd="\eOB"
"	set t_kl="\eOD"
"	set t_kr="\eOC"
endif


"------ 各種オプション設定
"--- modeline enable
set modeline
"--- Backup
set backup
"--- スワップファイルを作成しない
set noswapfile
"--- Visual選択で選択されたテキストが、自動的にクリップボードレジスタ "*" にコピーされる。
set guioptions+=a
"--- ツールバーを非表示にする。
set guioptions-=T
"--- メニューバーを非表示にする。
"set guioptions-=m
"--- 前回の検索パターンが存在するとき、それにマッチするテキストを全て強調表示する。
"--- （有効:hlsearch/無効:nohlsearch）
"highlight CursorLine term=reverse cterm=reverse
set hlsearch
"--- cindent と smartindent autoindento は排他
"set smartindent
set cindent

set nowrap
set number
"set cursorline
set smartcase
set incsearch
set showmode
set noshowmatch
set tabstop=4
set softtabstop=4
set shiftwidth=4
" tabでスペースを入力する
set expandtab
" 入力されているテキストの最大幅。行がそれより長くなると、
" この幅を超えないように空白の後で改行される。値を 0 に設定すると無効になる。
:set textwidth=0
"--- 検索がファイル末尾まで進んだら、ファイル先頭から再び検索する。（有効:wrapscan/無効:nowrapscan）
:set wrapscan
" ヤンクをクリップボードへ送り込む
"set clipboard+=unnamed
if !has('nvim')
    set clipboard=unnamed,autoselect
endif
"編集中でもバッファを切り替えれるようにしておく
set hidden
" ファイル保存ダイアログの初期ディレクトリをバッファファイル位置に設定
set browsedir=buffer
" カーソルを行頭、行末で止まらないようにする
set whichwrap=b,s,h,l,<,>,[,]
" バックスペースでインデントや改行を削除できるようにする
set backspace=indent,eol,start
"--- Tabブラウズ系
set showtabline=2
"--- 検索時に大文字小文字を無視
set ignorecase
"--- ファイル情報の表示
set laststatus=2
"--- normal org
"set statusline=%F%m%r%h%w\%=[TYPE=%Y]\[ASCII=\%03.3b]\ [HEX=\%02.2B]\ [EOL=%{&ff}]\ [ENC=%{&fileencoding}]\ [line=%l/%L][ROW=%04v][%p%%]
"--- new normal
set statusline=%F%m%r%h%w\ %{fugitive#statusline()}\%=\ \%b,\%Bh\ %{&ff},%{&fileencoding}\ %l/%L,%vR\ %P
"--- for fugitive
"set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P
"--- for simple
"set statusline=[TYPE=%Y]\[FORMAT=%{&ff}]\[ENC=%{&fileencoding}]\[LOW=%l/%L]
"--- 改行コードの自動認識
set fileformats=dos,unix,mac
"set list
"set listchars=tab:>.,trail:_,eol:$,extends:>,precedes:<,nbsp:%
set listchars=tab:>.,trail:_,extends:>


"-------  filetype 拡張子によるキーワード色分け
" XML
autocmd BufNewFile,BufRead *.vcxproj set filetype=xml
autocmd BufNewFile,BufRead *.cproject set filetype=xml
autocmd BufNewFile,BufRead *.project set filetype=xml
" Delphi/Pascal
autocmd BufNewFile,BufRead *.lpr set filetype=delphi
autocmd BufNewFile,BufRead *.pp set filetype=delphi
autocmd BufNewFile,BufRead *.mpas set filetype=delphi
autocmd BufNewFile,BufRead *.dpr set filetype=delphi
autocmd BufNewFile,BufRead *.pas set filetype=delphi
autocmd BufNewFile,BufRead *.dev set filetype=delphi
autocmd BufNewFile,BufRead *.PRG set filetype=delphi
" markdown
autocmd BufNewFile,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufRead *.wiki set filetype=markdown

autocmd BufNewFile,BufRead *.gembox set filetype=ruby
autocmd BufNewFile,BufRead *.pp set filetype=pascal
" assembler
autocmd BufNewFile,BufRead *.asm set filetype=asm68k
autocmd BufNewFile,BufRead *.dis set filetype=asm68k
autocmd BufNewFile,BufRead *.disas set filetype=asm68k
autocmd BufNewFile,BufRead *.disasm set filetype=armasm
" C language
autocmd BufNewFile,BufRead *.disasm set filetype=c
autocmd BufNewFile,BufRead *.plg set filetype=c
autocmd BufNewFile,BufRead *.lib set filetype=c
autocmd BufNewFile,BufRead *.txt set filetype=forth
autocmd! BufNewFile,BufRead *.c.org setlocal ft=c
autocmd BufNewFile,BufRead *.map set filetype=c
" ARM asm
autocmd BufNewFile,BufRead *.S set    filetype=armasm
autocmd BufNewFile,BufRead *.lss set  filetype=armasm
autocmd BufNewFile,BufRead *.lst set  filetype=armasm
autocmd BufNewFile,BufRead *.lst2 set  filetype=armasm
autocmd BufNewFile,BufRead *.lstx set filetype=armasm
autocmd BufNewFile,BufRead *.list set filetype=armasm
" BASIC
autocmd BufNewFile,BufRead *.gcb  set filetype=basic
autocmd BufNewFile,BufRead *.mbas set filetype=basic
autocmd BufNewFile,BufRead *.mbas set filetype=basic
autocmd BufNewFile,BufRead *.luna set filetype=basic
" arduino
autocmd! BufNewFile,BufRead *.pde setlocal ft=arduino
autocmd! BufNewFile,BufRead *.ino setlocal ft=arduino
" mruby
autocmd! BufNewFile,BufRead *.rbtmp setlocal ft=ruby
autocmd! BufNewFile,BufRead *.ctmp  setlocal ft=c
" makefile
autocmd! BufNewFile,BufRead Makefile setlocal ft=make
autocmd! BufNewFile,BufRead *.tmpl   setlocal ft=make
autocmd! BufNewFile,BufRead *.mak    setlocal ft=make
autocmd! BufNewFile,BufRead *.mk     setlocal ft=make
autocmd! BufNewFile,BufRead *.tpl    setlocal ft=make
autocmd FileType make setl noexpandtab
" .xyzzy
autocmd BufNewFile,BufRead *.xyzzy set filetype=lisp
" editra color syntax file *.ess
"autocmd! BufNewFile,BufRead *.ess setlocal ft=scss
" FORTH
autocmd BufNewFile,BufRead *.fth set filetype=forth
autocmd BufNewFile,BufRead *.f   set filetype=forth
"emacs
autocmd BufNewFile,BufRead *.spacemacs set filetype=lisp
"nim
autocmd BufNewFile,BufRead *.inc set filetype=nim
autocmd FileType nim setl tabstop=4 expandtab shiftwidth=4 softtabstop=4
"log"
autocmd BufNewFile,BufRead *.log set filetype=c

"------ マクロ、関数など
"--- make後にQuickFixウィンドウを自動的に開く方法
au QuickfixCmdPost make,grep,grepadd,vimgrep copen
"--- Python のインデント等の設定
autocmd FileType python setl autoindent
autocmd FileType python setl smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd FileType python setl tabstop=4 expandtab shiftwidth=4 softtabstop=4

syntax on

"--- 全角スペース・行末のスペース・タブの可視化 空白
if has("syntax")
    " PODバグ対策
    syn sync fromstart
    function! ActivateInvisibleIndicator()
        " 下の行の"　"は全角スペース
        syntax match InvisibleJISX0208Space "　" display containedin=ALL
        highlight InvisibleJISX0208Space term=underline ctermbg=Blue guibg=darkgray gui=underline
        "syntax match InvisibleTrailedSpace "[ \t]\+$" display containedin=ALL
        "highlight InvisibleTrailedSpace term=underline ctermbg=Red guibg=NONE gui=undercrl guisp=darkorange
        "syntax match InvisibleTab "\t" display containedin=ALL
        "highlight InvisibleTab term=underline ctermbg=white gui=undercurl guisp=darkslategray
	endf
    augroup invisible
        autocmd! invisible
        autocmd BufNew,BufRead * call ActivateInvisibleIndicator()
    augroup END
endif

"--- 最後のカーソル位置を復元する
" https://github.com/osyo-manga/vim-over
if has("autocmd")
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line ("'\"") <= line("$") |
    \   exe "normal! g'\"" |
    \ endif
endif
"--- 挿入モードを抜けたら保存する
autocmd InsertLeave * :w

" http://kannokanno.hatenablog.com/entry/2014/01/04/212725
"augroup Vimrc
"  autocmd!
"  autocmd InsertLeave * call <SID>auto_save()
"  function! s:auto_save()
"    if filewritable(expand('%'))
"      write
"    endif
"  endfunction
"augroup END

"--- .h 作成時に自動的に #ifndef 〜 #endif を挿入する
au BufNewFile *.h call IncludeGuard()
function! IncludeGuard()
   let fl = getline(1)
   if fl =~ "^#if"
       return
   endif
   let gatename = substitute(toupper(expand("%:t")), "??.", "_", "g")
   normal! gg
   execute "normal! i#ifndef " . gatename . "_INCLUDED"
   execute "normal! o#define " . gatename .  "_INCLUDED"
   execute "normal! Go#endif   /* " . gatename . "_INCLUDED */"
   4
endfunction
"--- ファイルをD&Dした時に新Tabで開く. This functions interfers Explore(Filer) of vim.
"autocmd VimEnter * tab all
"autocmd BufAdd * exe 'tablast | tabe "' . expand( "<afile") .'"'
"--- v で範囲選択して # キーを押すとその範囲を#ifdef 〜 #endif で囲む。
function! InsertIfdef() range
	let sym = input("symbol:")
	call append(a:firstline-1, "#ifdef " . sym)
	call append(a:lastline+1, "#endif // " . sym)
endfunction
vnoremap # :call InsertIfdef()<CR>
"--- ディレクトリ自動移動 (開いたファイルのディレクトリがカレントディレクトリに)
"au   BufEnter *   execute ":lcd " . expand("%:p:h")
"--- ファイルの存在するディレクトリをカレントディレクトリにする
	command!  Cda execute ":cd " . expand("%:p:h")
	"command!  Cdl execute ":lcd " . expand("%:p:h")

"------ Cygwin only: カーソルキーがおかしい対応
if has("win32unix")
set mouse=a
"if has("win32unix")
	" For Arrow keys
"set ttimeoutlen=100
"	set term=ansi
	set t_ku=OA
	set t_kd=OB
	set t_kl=OD
	set t_kr=OC
"	set t_ku="\eOA"
"	set t_kd="\eOB"
"	set t_kl="\eOD"
"	set t_kr="\eOC"
endif
"------ 挿入モード時、ステータスラインの色を変更
"https://sites.google.com/site/fudist/Home/vim-nihongo-ban/vim-color#color-insertmode
let g:hi_insert = 'highlight StatusLine guifg=darkblue guibg=darkyellow gui=none ctermfg=blue ctermbg=yellow cterm=none'
if has('syntax')
  augroup InsertHook
	autocmd!
	autocmd InsertEnter * call s:StatusLine('Enter')
	autocmd InsertLeave * call s:StatusLine('Leave')
  augroup END
endif
let s:slhlcmd = ''
function! s:StatusLine(mode)
  if a:mode == 'Enter'
	silent! let s:slhlcmd = 'highlight ' . s:GetHighlight('StatusLine')
	silent exec g:hi_insert
  else
	highlight clear StatusLine
	silent exec s:slhlcmd
  endif
endfunction
function! s:GetHighlight(hi)
  redir => hl
  exec 'highlight '.a:hi
  redir END
  let hl = substitute(hl, '[\r\n]', '', 'g')
  let hl = substitute(hl, 'xxx', '', '')
  return hl
endfunction
"------ フォント設定の切り替え
" 0123456789
" abcdefghijklmnopqrstuvwxyz
" ABCDEFGHIJKLMNOPQRSTUVWXYZ
" "a_b_c",'d-e---_______--f-',
" uint8_t spi_send_char(void){}
" i++; j--; if( i >= 0 ) ptr->count++;
" ○◎○△□ 日本語の入力テスト
" int, long, junk, OOp, 00p, loop,
" 100, 1,000, 1.2i,var, for( I=0 ),
" while( i++ ); Open, fopen, open0
" *prt += 0.1; bin &= 0x2AFD;
" i |= lpj--;
"
nnoremap <Space>1  :call Font1(13)<CR>
nnoremap <Space>2  :call Font2(14)<CR>
nnoremap <Space>3  :call Font3(14)<CR>
nnoremap <Space>4  :call Font4(12)<CR>
nnoremap <Space>5  :call Font5(14)<CR>
nnoremap <Space>6  :call Font6(14)<CR>
nnoremap <Space>7  :call Font7(14)<CR>
" 1
let g:msGothic		= "MS_Gothic"
let g:kochiGothic	= "Kochi_Gothic"
let g:monoSpace		= "Monospace"
" 2
let g:rictyDim		= "Ricty_Diminished"
" 3
let g:rictyDimDis	= "Ricty_Diminished_Discord"
" 4
let g:scpLight		= "Source_Code_Pro_Light"
" 5
let g:consolas		= "Consolas"
" 6
let g:myrica		= "Myrica M"
" 7
let g:myricaM		= "MyricaM M"

function! GetFontSize(size)
    if has('win32') || has('win64')
		return ":h" . a:size
	else
		return "\ " .a:size
	endif
endfunction

function! Font1(size)
    if has('win32') || has('win64')
        let		&guifont	= g:msGothic	. GetFontSize(a:size)
        let		&guifontwide= g:msGothic	. GetFontSize(a:size)
	else
		set guifontwide=東風ゴシック\ 12
		set guifont=Monospace\ 12
"        let		&guifont	= g:monoSpace   . GetFontSize(a:size)
 "       let		&guifontwide= g:kochiGothic . GetFontSize(a:size)
	endif
endfunction
function! Font2(size)
        let		&guifont	= g:rictyDim	. GetFontSize(a:size)
        let		&guifontwide= g:rictyDim	. GetFontSize(a:size)
endfunction
function! Font3(size)
        let		&guifont	= g:rictyDimDis . GetFontSize(a:size)
        let		&guifontwide= g:rictyDimDis . GetFontSize(a:size)
endfunction
function! Font4(size)
        let		&guifont	= g:scpLight	. GetFontSize(a:size)
        let		&guifontwide= g:rictyDimDis . GetFontSize(a:size)
endfunction
function! Font5(size)
        let		&guifont	= g:consolas	. GetFontSize(a:size)
        let		&guifontwide= g:msGothic	. GetFontSize(a:size)
endfunction
function! Font6(size)
        let		&guifont	= g:myrica	    . GetFontSize(a:size)
        let		&guifontwide= g:myrica		. GetFontSize(a:size)
endfunction
function! Font7(size)
        let		&guifont	= g:myricaM	    . GetFontSize(a:size)
        let		&guifontwide= g:myricaM		. GetFontSize(a:size)
endfunction
"set renderoptions=type:directx
"set antialias


" Syntastic
"if g:syntastic_en==1
"    set statusline+=%#warningmsg#
"    set statusline+=%{SyntasticStatuslineFlag()}
"    set statusline+=%*
"
"    let g:syntastic_always_populate_loc_list = 1
"    let g:syntastic_auto_loc_list = 1
"    "---------
"    let g:syntastic_check_on_open = 1
"    let g:syntastic_check_on_wq = 0
"    let g:syntastic_enable_signs=1
"    "---------
"    "let g:syntastic_c_compiler_options="-I../lib -I../sys -D=STM32F030x8 -I../Inc -I../Drivers/STM32F0xx_HAL_Driver/Inc  -I../Drivers/CMSIS/Device/ST/STM32F0xx/Include -I../Drivers/CMSIS/Include"
"endif


noremap <Leader>s :FixWhitespace<CR>
set diffopt-=filler

" for  'yegappan/grep'
if has('win32') || has('win64')
    let Grep_Path =       'd:\msys32\usr\bin\grep.exe'
    let Grep_Xargs_Path = 'd:\msys32\usr\bin\xargs.exe'
    let Grep_Find_Path =  'd:\msys32\usr\bin\find.exe'
    let Grep_Shell_Quote_Char = '"'
endif

if has('nvim')
    "------------------- deoplete setting ---------------- start
    let g:deoplete#enable_at_startup = 1
    "------------------- deoplete setting ---------------- end
endif


"------------------- neosnippet setting ----------------start
" Plugin key-mappings.
"imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"smap <C-k>     <Plug>(neosnippet_expand_or_jump)
"xmap <C-k>     <Plug>(neosnippet_expand_target)

"" SuperTab like snippets behavior.
"imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)"
"\: pumvisible() ? "\<C-n>" : "\<TAB>"
"smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)"
"\: "\<TAB>"

" For snippet_complete marker.
"if has('conceal')
"  set conceallevel=2 concealcursor=i
"endif
"------------------- neosnippet setting ----------------end

if !exists("g:quickrun_config")
    let g:quickrun_config = {}
endif

autocmd BufNewFile,BufRead *.crs setf rust
autocmd BufNewFile,BufRead *.rs  let g:quickrun_config.rust = {
\ 'runner/vimproc/updatetime'              : 50,
\ 'hook/cd/directory'              : '%S:p:h',
\ 'outputter/buffer/split'                 : 'botright 8sp',
\ 'hook/close_quickfix/enable_hook_loaded' : 1,
\ 'outputter/quickfix/errorformat' :
\ ',' .
\ '%-G,' .
\ '%-Gerror: aborting %.%#,' .
\ '%-Gerror: Could not compile %.%#,' .
\ '%Eerror: %m,' .
\ '%Eerror[E%n]: %m,' .
\ '%-Gwarning: the option `Z` is unstable %.%#,' .
\ '%Wwarning: %m,' .
\ '%Inote: %m,' .
\ '%C %#--> %f:%l:%c'
\,
\'exec' : 'cargo run'
\}
autocmd BufNewFile,BufRead *.crs let g:quickrun_config.rust = {
\                               'exec' : 'cargo script %s -- %a'
\                                }






"""""""" カラースキーム表示コマンド 開始
"" 参考 : http://cohama.hateblo.jp/entry/2013/08/11/020849
" :SyntaxInfo で表示
"
function! s:get_syn_id(transparent)
  let synid = synID(line("."), col("."), 1)
  if a:transparent
    return synIDtrans(synid)
  else
    return synid
  endif
endfunction
function! s:get_syn_attr(synid)
  let name = synIDattr(a:synid, "name")
  let ctermfg = synIDattr(a:synid, "fg", "cterm")
  let ctermbg = synIDattr(a:synid, "bg", "cterm")
  let guifg = synIDattr(a:synid, "fg", "gui")
  let guibg = synIDattr(a:synid, "bg", "gui")
  return {
        \ "name": name,
        \ "ctermfg": ctermfg,
        \ "ctermbg": ctermbg,
        \ "guifg": guifg,
        \ "guibg": guibg}
endfunction
function! s:get_syn_info()
  let baseSyn = s:get_syn_attr(s:get_syn_id(0))
  echo "name: " . baseSyn.name .
        \ " ctermfg: " . baseSyn.ctermfg .
        \ " ctermbg: " . baseSyn.ctermbg .
        \ " guifg: " . baseSyn.guifg .
        \ " guibg: " . baseSyn.guibg
  let linkedSyn = s:get_syn_attr(s:get_syn_id(1))
  echo "link to"
  echo "name: " . linkedSyn.name .
        \ " ctermfg: " . linkedSyn.ctermfg .
        \ " ctermbg: " . linkedSyn.ctermbg .
        \ " guifg: " . linkedSyn.guifg .
        \ " guibg: " . linkedSyn.guibg
endfunction
command! SyntaxInfo call s:get_syn_info()
"""""""" カラースキーム表示コマンド 終了

" 以下のコマンドは :colorscheme の前に設定します
" コメントを濃い緑にする
"autocmd ColorScheme * highlight Comment    ctermfg=red guifg=cyan
"autocmd ColorScheme * highlight Identifier ctermfg=green  guifg=green
"autocmd ColorScheme * highlight Statement  ctermfg=green  guifg=green

" ファイルの変更チェック
"http://vim-jp.org/vim-users-jp/2011/03/12/Hack-206.html
augroup vimrc-checktime
  autocmd!
  autocmd WinEnter * checktime
augroup END


"if has('nvim')        " True color in neovim wasn't added until 0.1.5
    set termguicolors
"endif
" カラー設定: ファイル名を指定する
set t_Co=256
colorscheme dos-color

" Help項目をマウスでジャンプする
set mouse=a

" Python 指定
if has('win32') || has('win64')
    let g:python3_host_prog = expand('d:/python386/python.exe')
else
    let g:python3_host_prog = expand('/bin/python3')
endif

" vimdiff時 次の差分、前の差分
"nnoremap <C-k>	[c
"nnoremap <C-j>	]c

"---------------------for nim pulgin -
fun! JumpToDef()
  if exists("*GotoDefinition_" . &filetype)
    call GotoDefinition_{&filetype}()
  else
    exe "norm! \<C-]>"
  endif
endf

" Jump to tag
nnoremap <M-g> :call JumpToDef()<cr>
inoremap <M-g> <esc>:call JumpToDef()<cr>i
"-------------------------

" ---- Open URL Web
let g:netrw_nogx = 1 " disable netrw's gx mapping.
nmap gj <Plug>(openbrowser-smart-search)
vmap gj <Plug>(openbrowser-smart-search)

" window move
" https://liginc.co.jp/409849
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"------------ my macro
"  1行目 テキスト、 2行目 URL を 1行のhrefリンクに変換
let @h ='ddpkI<a href="A">jjJxA>/bb</a>0j'
let @m =':%s///g'

"------------ messages の内容をクリップボードにコピー
command MesToClipboard let @+ = execute("messages")
command! MesDeSearch call system("xdg-open \"https://www.duckduckgo.com/?q=" . execute("1messages") . "\"")


"let g:use_xhtml = 0
let g:html_use_css = 1
let g:html_no_pre = 0
let g:html_number_lines = 0
"let g:html_font = 'monospace'


