source /etc/profile

set -o vi
export LANG=ja_JP.SJIS
export LANGUAGE=ja
export HISTCONTROL=ignoredups 
export HISTIGNORE="history*"

alias waf=$PWD/waf
alias ll='ls -al --color --show-control-chars'
alias l='ls -al --color --show-control-chars'
alias vi='vim'
alias cc='gcc'
alias hg='history | grep -i '

alias dmruby='cd work/mruby-data/cm3'

#export PATH=$PATH:/f/MinGW/bin:/d/TortoiseHg:/c/doxygen
#:/e/lua/5.1:/c/Program\ Files/HTML\ Help\ Workshop:
#.:/d/Microchip/MPLAB\ C32\ Suite/bin:/g/gvim/global/bin

#export PS1="msys:\w \[\n\]$ "


alias m='make'
alias mc='make clean'
alias md='make depend'
alias mm='mc; md; m'
alias cf='echo ../configure -T _gcc >> .bash_history'
alias rc='rm *; ../configure -T  _gcc ; make depend ; make'
alias cb='cd build'
alias gv='arm-none-eabi-gcc -v'

export PS1='\033[1;32m\]\s: \[\033[1;33m\][ `echo \w | sed -r "s%^.*/(.*/.*$)%\1%"` ]\033[0m\]\[\n\]\[\033[1;36m\]$\[\033[1;37m\] '
#export PS1="\s: \w \[\n\]$ "
#export PS1='\033[32m\]\s: \[\033[33m\w\033[0m\]\[\n\]$ '

LS_COLORS="di=36;01:ex=32;01:ln=33;01:ow=04;31:tw=04;31"
export LS_COLORS

export PATH=$PATH:/c/00emacs-home/vimtool:/p/hg-root/pinguino/x3-new/win32/p32/bin
export PATH=/g/Git/bin:/h/yagartox/bin:$PATH
#export PATH=/h/yagartox/bin:$PATH
#export PATH=/d/arm_emd_4.6_2012q4/bin:$PATH
export PATH=/d/arm_emb_4.7.4_2013q2/bin:$PATH
#export PATH=$PATH:/p/maple-work/maple-ide-0.0.12-windowsxp32/hardware/tools/arm/bin
export PATH=$PATH:/c/Python26:/c/Python26/Scripts
#export PATH=$PATH:/d/SourceryXpresso/bin
export PATH=$PATH:/d/Microsoft_Visual_Studio_10.0/Common7/IDE
export PATH=$PATH:/d/drv_e/hg-root/ccache
#export PATH=$PATH:/d/Ruby-1.9.1/bin
#export PATH=$PATH:/d/ruby187/bin
export PATH=$PATH:/d/ruby-2.0.0/bin
export PATH=$PATH:/c/Qt-4.7.3-dev-lite-msvc2010-rs/bin

# for ccache
export CCACHE_DIR=/c/00emacs-home/.ccache
 export  LIB_MAPLE_HOME=../../../../libmaple-vld

 cd $MOV_FOLDER

