" .vimrc file
" Maintainer:	audin 
" Last Change:	2014/10

" このファイル自身のエンコードは「utf-8」、改行コードはLF。
set fileformat=unix
set fileencoding=sjis
set enc=japan


" フォント 1位：MSゴシック・標準・１２
"         ２位: FixedSys・標準・１３ 
"
"
" os = win32, unix, win32unix
"
"
syntax on

let g:unite = 0
" スワップファイルを作成しない
":set noswapfile

" カラー設定: ファイル名を指定する
colorscheme dos-color

" FileType plugin
filetype plugin indent on

" プラグイン管理 pathogen.vim

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()


" エディット関連
"
" Visual選択で選択されたテキストが、自動的にクリップボードレジスタ "*" にコピーされる。
:set guioptions+=a


"highlight CursorLine term=reverse cterm=reverse
" 前回の検索パターンが存在するとき、それにマッチするテキストを全て強調表示する。（有効:hlsearch/無効:nohlsearch）
set hlsearch 
"set smartindent
set cindent
set nowrap
set number
set cursorline
set smartcase
set incsearch
set showmode
set tabstop=4
set softtabstop=4
set shiftwidth=4
" 入力されているテキストの最大幅。行がそれより長くなると、この幅を超えないように空白の後で改行される。値を 0 に設定すると無効になる。
:set textwidth=0

" 新しい行のインデントを現在行と同じにする
set autoindent
" 検索がファイル末尾まで進んだら、ファイル先頭から再び検索する。（有効:wrapscan/無効:nowrapscan）
:set wrapscan

" ディレクトリ自動移動 (開いたファイルのディレクトリがカレントディレクトリに)
"au   BufEnter *   execute ":lcd " . expand("%:p:h")
" ファイルの存在するディレクトリをカレントディレクトリにする
command!  Cda execute ":cd " . expand("%:p:h")
command!  Cdl execute ":lcd " . expand("%:p:h")

" 最後に変更されたテキストを選択する
"nnoremap gc  `[v`]

" Tabブラウズ系
set showtabline=2 
" 検索時に大文字小文字を無視
set ignorecase
"ファイル情報の表示
set laststatus=2
set statusline=%F%m%r%h%w\%=[TYPE=%Y]\[ASCII=\%03.3b]\ [HEX=\%02.2B]\ [EOL=%{&ff}]\ [ENC=%{&fileencoding}]\ [line=%l/%L][ROW=%04v][%p%%]
"set statusline=[TYPE=%Y]\[FORMAT=%{&ff}]\[ENC=%{&fileencoding}]\[LOW=%l/%L]

"改行コードの自動認識
set fileformats=dos,unix,mac

"set list
"set listchars=tab:>.,trail:_,eol:$,extends:>,precedes:<,nbsp:%
set listchars=tab:>.,trail:_,extends:>

" 全角スペース・行末のスペース・タブの可視化
if has("syntax")
    syntax on
 
    " PODバグ対策
    syn sync fromstart
 
    function! ActivateInvisibleIndicator()
        " 下の行の"　"は全角スペース
        syntax match InvisibleJISX0208Space "　" display containedin=ALL
        highlight InvisibleJISX0208Space term=underline ctermbg=Blue guibg=darkgray gui=underline
        "syntax match InvisibleTrailedSpace "[ \t]\+$" display containedin=ALL
        "highlight InvisibleTrailedSpace term=underline ctermbg=Red guibg=NONE gui=undercurl guisp=darkorange
        "syntax match InvisibleTab "\t" display containedin=ALL
        "highlight InvisibleTab term=underline ctermbg=white gui=undercurl guisp=darkslategray
	endf
    augroup invisible
        autocmd! invisible
        autocmd BufNew,BufRead * call ActivateInvisibleIndicator()
    augroup END
endif


" ヤンクをクリップボードへ送り込む
set clipboard=unnamed

"編集中でもバッファを切り替えれるようにしておく
set hidden

"ルーラーを表示
set ruler
set title

" ファイル保存ダイアログの初期ディレクトリをバッファファイル位置に設定
set browsedir=buffer 

" スワップファイル用のディレクトリ
set writebackup 
if has("win32")
	set backupdir=$HOME\vimbackup
	set directory=$HOME\vimbackup
else
	set backupdir=$HOME/vimbackup
	set directory=$HOME/vimbackup
endif

" カーソルを行頭、行末で止まらないようにする
set whichwrap=b,s,h,l,<,>,[,]

" バックスペースでインデントや改行を削除できるようにする
set backspace=indent,eol,start

" ctags view
if has("win32unix")
	let g:showfuncctagsbin = "/cygdrive/c/00emacs-home/ctags"
else
	let g:showfuncctagsbin = "c:\\00emacs-home\\vimtool\\ctags.exe"
endif
"nnoremap  <C-3>	<Plug>ShowFunc
"
" .h 作成時に自動的に #ifndef 〜 #endif を挿入する
au BufNewFile *.h call IncludeGuard()
function! IncludeGuard()
   let fl = getline(1)
   if fl =~ "^#if"
       return
   endif
   let gatename = substitute(toupper(expand("%:t")), "??.", "_", "g")
   normal! gg
   execute "normal! i#ifndef " . gatename . "_INCLUDED"
   execute "normal! o#define " . gatename .  "_INCLUDED"
   execute "normal! Go#endif   /* " . gatename . "_INCLUDED */"
   4
endfunction 

" ファイルをD&Dした時に新Tabで開く. This functions interfers Explore(Filer) of vim. 
"autocmd VimEnter * tab all
"autocmd BufAdd * exe 'tablast | tabe "' . expand( "<afile") .'"'

" v で範囲選択して # キーを押すとその範囲を#ifdef 〜 #endif で囲む。
function! InsertIfdef() range
	let sym = input("symbol:")
	call append(a:firstline-1, "#ifdef " . sym) 
	call append(a:lastline+1, "#endif // " . sym)
endfunction
vnoremap # :call InsertIfdef()<CR>


" .vimrcを開く。専用ホットキーを定義する場合
nnoremap <Space>.  :<C-u>edit $MYVIMRC<CR>
" source ~/.vimrc を実行する。
nnoremap <Space>,  :<C-u>source $MYVIMRC<CR> 

" ファイル保存のショートカット
"nnoremap <Space>s
"\    :<C-u>w<CR>



" tag jump
"nnoremap t  <Nop>
""「飛ぶ」
"nnoremap tt  <C-]>        
"  "「進む」
"nnoremap tj  :<C-u>tag<CR>
"  "「戻る」
"nnoremap tk  <C-o>
" "履歴一覧
"nnoremap tl  :<C-u>tags<CR> 

" vimdir の　mercurialのための設定。 for vimdiff
let g:DirDiffDynamicDiffText = 1"

" grep 設定
if has("win32")
	set grepprg=C:\00emacs-home\jvgrep-win32-1.7\jvgrep.exe
else
	set grepprg=grep
endif

" grep.vim 設定
" http://blog.blueblack.net/item_199
if has("win32")
	let Grep_Path       = 'd:\MinGW\msys\1.0\bin\grep.exe'
	let Fgrep_Path      = 'd:\MinGW\msys\1.0\bin\grep.exe -F'
	let Egrep_Path      = 'd:\MinGW\msys\1.0\bin\grep.exe -E'
	let Grep_Find_Path  = 'd:\MinGW\msys\1.0\bin\find.exe'
	let Grep_Xargs_Path = 'd:\MinGW\msys\1.0\bin\xargs.exe'
"let Grep_Find_Use_Xargs = 0
else
	let Grep_Path       = 'grep'
    let Fgrep_Path      = 'grep -F'
    let Egrep_Path      = 'grep -E'
    let Grep_Find_Path  = 'find'
    let Grep_Xargs_Path = 'xargs'
endif

let Grep_Shell_Quote_Char = '"'
"検索除外フォルダ
let Grep_Skip_Dirs = '.svn .hg'
let Grep_Skip_Files = '*.bak *~'

" filetype
autocmd BufNewFile,BufRead *.pp set filetype=pascal
autocmd BufNewFile,BufRead *.dis set filetype=asm68k
autocmd BufNewFile,BufRead *.S set filetype=asm68k
autocmd BufNewFile,BufRead *.lss set filetype=asm68k
autocmd BufNewFile,BufRead *.map set filetype=c
autocmd! BufNewFile,BufRead *.c.org setlocal ft=c
" .xyzzy
autocmd BufNewFile,BufRead *.xyzzy set filetype=lisp
" arduino
autocmd! BufNewFile,BufRead *.pde setlocal ft=arduino
autocmd! BufNewFile,BufRead *.ino setlocal ft=arduino
" mruby
autocmd! BufNewFile,BufRead *.rbtmp setlocal ft=ruby
autocmd! BufNewFile,BufRead *.ctmp setlocal ft=c

" editra color syntax file *.ess
"autocmd! BufNewFile,BufRead *.ess setlocal ft=scss

"http://sites.google.com/site/fudist/Home/vim-nihongo-ban/tips#TOC-9
"ciyはカーソル位置の単語をヤンクした文字列に置き換えます。
"ciyはテキストオブジェクトなのでカーソルが単語内の何処にあっても使えます。
"cyはカーソル位置以降の単語部分をヤンクした単語に置換します。
"
"一度cyやciyを実行した後、n.で繰り返せる用に、検索レジスタが置換された文字列と置き換わります。
nnoremap <silent> ciy ciw<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
nnoremap <silent> cy   ce<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
vnoremap <silent> cy   c<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
"    置換に使いたい文字列をヤンクしておきます。
"    置換したい単語まで移動します。
"    cyかciyを実行します。
"    カーソル位置の単語がヤンクした文字列に置換されます。
"    必要ならn.で繰り返します。
"同様にヤンクした文字を貼り付けます。
nnoremap gy "0P
"この場合はカーソル位置の前に挿入したいことが多いので、Pで貼り付けています。
"単純にpだとレジスタの内容が入れ替わっていることも多いのでヤンクレジスタを指定して貼り付けます。



"http://sites.google.com/site/fudist/Home/vim-nihongo-ban/tips#TOC-9
"バッファ変更時のみ保存
"nnoremap <silent> <C-s> :<C-u>update<CR>
nnoremap <silent> <Space>s :<C-u>update<CR>

" IME 無効化
"set imsearch=-1
if ($OSTYPE!='cygwin') && ($OSTYPE!='msys')
	if has("win32")
		inoremap <ESC> <ESC>:set iminsert=0<CR>  " ESCでIMEを確実にOFF
	endif
endif


" :make の出力を実行中に見たい
"set shellpipe=\|\ tee

" Gtags
nnoremap <C-n> :cn<CR> " 次の検索結果へジャンプする
nnoremap <C-p> :cp<CR> " 前の検索結果へジャンプする
nnoremap <C-j> :GtagsCursor<CR> " カーソル位置の関数にジャンプする
nnoremap <C-g> :Gtags " Gtags と入力する 
nnoremap <C-i> :Gtags -f %<CR> " 開いているファイルに定義されている関数の一覧を表示

" make後にQuickFixウィンドウを自動的に開く方法 
au QuickfixCmdPost make,grep,grepadd,vimgrep copen
"
" Python のインデント等の設定
autocmd FileType python setl autoindent
autocmd FileType python setl smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd FileType python setl tabstop=8 expandtab shiftwidth=4 softtabstop=4

" neocomplcache settings
" neocomlcache enable
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_enable_insert_char_pre = 1
if g:unite==1
	" unite
	nnoremap <silent><Space>l	:Unite buffer<CR>
	" ---  vim filer ---
	nnoremap <silent><Space>j  :VimFilerBufferDir<CR>
	let g:vimfiler_as_default_explorer = 1
	" Edit file by tabedit.
	let g:vimfiler_edit_action = 'tabopen'
	nnoremap <silent><Space>m  :Unite file_mru<CR>
else
	" ---  vim filer ---
	nnoremap <silent><Space>j	:Explore<CR>
	nnoremap <silent><Space>l	:Bufferlist<CR>
	nnoremap <silent><Space>m  :MRU<CR>
	let MRU_Auto_Close = 0
endif

" 次のバッファ、前のバッファ、バッファの削除、バッファのリスト
nnoremap <silent><Space>b	:bp<CR>
nnoremap <silent><Space>n	:bn<CR>
nnoremap <silent><Space>k	:bd<CR>
nnoremap <Space>o	<C-W>o				" カーソルのあるウインドウを最大化する
nnoremap <silent><Space>h	:hide<CR>	" カーソルのあるウインドウを隠す

nnoremap <silenti><Leader>y	:Unite -buffer-name=register register<CR>
"nnoremap <silent> <Space>m  :CtrlPMRU<CR>

" Change directory
nnoremap <Space>c	:Cda<CR>

if has("win32unix")
" Cygwin only: カーソルキーがおかしい対応
set mouse=a
" カーソルキーがおかしい対応
set t_ku=<Up> t_kd=<Down> t_kl=<Right> t_kr=<Left>
noremap OA <Up>
noremap! OA <Up>
noremap OB <Down>
noremap! OB <Down>
noremap OC <Right>
noremap! OC <Right>
noremap OD <Left>
noremap! OD <Left>
"	set nocompatible
"	set term=ansi

"if has("win32unix")
	" For Arrow keys 
	set nocompatible
"set ttimeoutlen=100	
"	set term=ansi
	set t_ku=OA
	set t_kd=OB
	set t_kl=OD
	set t_kr=OC
"	set t_ku="\eOA"
"	set t_kd="\eOB"
"	set t_kl="\eOD"
"	set t_kr="\eOC"
if $OSTYPE=='msys'
" カーソルキーがおかしい対応
set t_ku=<Up> t_kd=<Down> t_kl=<Right> t_kr=<Left>
noremap OA <Up>
noremap! OA <Up>
noremap OB <Down>
noremap! OB <Down>
noremap OC <Right>
noremap! OC <Right>
noremap OD <Left>
noremap! OD <Left>
"	set nocompatible
"	set term=ansi
else
if $OSTYPE=='cygwin'
	" For Arrow keys 
	"set t_ku=OA
	"set t_kd=OB
	"set t_kl=OD
	"set t_kr=OC

"	set nocompatible
"	set term=ansi
endif
endif
endif

" カーソル上の単語をヤンク、単語範囲選択
nnoremap <Space>t    viw


" カーソル上の単語を Gtagsする
nnoremap <Space>g	:Gtags -ig <C-R><C-W><CR>

" Tag page
nnoremap <S-TAB>	:tabn<CR>
nnoremap <C-TAB>	:gT<CR>

" vcscommand の割り当て
noremap <Space>d	:VCSVimDiff<CR>

" vimshell
noremap <Space>v	:VimShellPop %:p:h<CR>
nnoremap <silent> ,ug :Unite grep:%:-iHRn<CR>


" taglist
let Tlist_Show_One_File = 1
"let Tlist_Use_Right_Window = 1
let Tlist_Exit_OnlyWindow = 1

nnoremap <Leader>w  :set wrap!<CR>
noremap <Leader>\ <C-W>w
noremap <Leader>] <C-]>
noremap <Leader>o <C-o>      
noremap <Leader>f :TlistToggle<CR>		"taglist key
noremap <Leader>tr :TrinityToggleAll<CR>

""""""""""""""""""""""""""""""
"挿入モード時、ステータスラインの色を変更
""""""""""""""""""""""""""""""
"https://sites.google.com/site/fudist/Home/vim-nihongo-ban/vim-color#color-insertmode

"let g:hi_insert = 'highlight StatusLine guifg=yellow guibg=#6666dd gui=none ctermfg=black ctermbg=yellow cterm=none'
"let g:hi_insert = 'highlight StatusLine guifg=darkblue guibg=darkyellow gui=none ctermfg=blue ctermbg=yellow cterm=none'
let g:hi_insert = 'highlight StatusLine guifg=darkblue guibg=darkyellow gui=none ctermfg=blue ctermbg=yellow cterm=none'

if has('syntax')
	augroup InsertHook
		autocmd!
		autocmd InsertEnter * call s:StatusLine('Enter')
		autocmd InsertLeave * call s:StatusLine('Leave')
	augroup END
endif

let s:slhlcmd = ''
function! s:StatusLine(mode)
	if a:mode == 'Enter'
		silent! let s:slhlcmd = 'highlight ' .  s:GetHighlight('StatusLine')
		silent exec g:hi_insert
	else
		highlight clear StatusLine
		silent exec s:slhlcmd
	endif
endfunction

function! s:GetHighlight(hi)
	redir => hl
	exec 'highlight '.a:hi
	redir END
	let hl = substitute(hl, '[\r\n]', '', 'g')
	let hl = substitute(hl, 'xxx', '', '')
	return hl
endfunction

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                                                              "
" The_setting_example_in_my_vimrc_file:-)                                      "
"                                                                              "
" // The switch of the Source Explorer                                         "
" nmap <F8> :SrcExplToggle<CR>
"                                                                              "
" // Set the height of Source Explorer window                                  "
" let g:SrcExpl_winHeight = 8
"                                                                              "
" // Set 100 ms for refreshing the Source Explorer                             "
" let g:SrcExpl_refreshTime = 100
"                                                                              "
" // Set "Enter" key to jump into the exact definition context                 "
" let g:SrcExpl_jumpKey = "<ENTER>"
"                                                                              "
" // Set "Space" key for back from the definition context                      "
" let g:SrcExpl_gobackKey = "<SPACE>"
"                                                                              "
" // In order to Avoid conflicts, the Source Explorer should know what plugins "
" // are using buffers. And you need add their bufname into the list below     "
" // according to the command ":buffers!"                                      "
 let g:SrcExpl_pluginList = [
         \ "__Tag_List__",
         \ "_NERD_tree_",
         \ "Source_Explorer"
     \ ]
                                                                              "
" // Enable/Disable the local definition searching, and note that this is not  "
" // guaranteed to work, the Source Explorer doesn't check the syntax for now. "
" // It only searches for a match with the keyword according to command 'gd'   "
" let g:SrcExpl_searchLocalDef = 1
"                                                                              "
" // Do not let the Source Explorer update the tags file when opening          "
" let g:SrcExpl_isUpdateTags = 0
"                                                                              "
" // Use 'Exuberant Ctags' with '--sort=foldcase -R .' or '-L cscope.files' to "
" //  create/update a tags file                                                "
 let g:SrcExpl_updateTagsCmd = "ctags --sort=foldcase -R ."
"                                                                              "
" // Set "<F12>" key for updating the tags file artificially                   "
 let g:SrcExpl_updateTagsKey = "<F12>"
"                                                                              "
" Just_change_above_of_them_by_yourself:-)                                     "
"                                                                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"eregex.vim
"nnoremap / :M/
"nnoremap ./ /

" yank ring
nnoremap <silent> <Space>y	:YRShow<CR>

" :copen, :cclose
let s:quickfixwindow = "close"
function! b:openCloseQuickfix()
	if "open" ==? s:quickfixwindow
		let s:quickfixwindow = "close"
		cclose
	else
		let s:quickfixwindow = "open"
		copen
	endif
endfunction

nmap <silent> <Leader>p :call b:openCloseQuickfix()<CR>
imap <silent> <Leader>p <C-o>:call b:openCloseQuickfix()<CR>


nnoremap <Space>/	:make<CR><CR>
nnoremap m <PageUp> 
"nnoremap ? <PageDown> 

nnoremap vf ][v[[?^?s*$<CR>		" select function on C source code
nnoremap vb /{<CR>%v%0          " select for , while loop on C source code 

"let g:neocomplcache_omni_patterns.python = ''
"
