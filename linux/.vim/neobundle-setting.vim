"""""""""""""""""""""""""""""""""""""""""""""""
"
" Neobundle settings : 
"
"""""""""""""""""""""""""""""""""""""""""""""""
" Start [ Neobundle ]
filetype off
if has('vim_starting')
	if has("win32")
		set runtimepath+=~/vimfiles/bundle/neobundle.vim
		call neobundle#begin(expand('~/vimfiles/bundle'))
	else
		set runtimepath+=~/.vim/bundle/neobundle.vim
		call neobundle#begin(expand('~/.vim/bundle'))
	endif
endif
NeoBundleFetch 'Shougo/neobundle.vim'

"  Listing my plugin
if g:unite==1
	NeoBundle 'Shougo/unite.vim'
	NeoBundle 'Shougo/neomru.vim'
	NeoBundle 'Shougo/vimfiler.vim'
endif
NeoBundle 'sandeepcr529/Buffet.vim'
NeoBundle 'https://github.com/dinau/mru.vim.git'
NeoBundle 'tpope/vim-surround'
NeoBundle 'vim-scripts/YankRing.vim'
NeoBundle 'nathanaelkane/vim-indent-guides'
"---  neocomplcache
if g:neocomple==1
	let g:neocomplcache_enable_at_startup = 1
	let g:neocomplcache_enable_insert_char_pre = 1
	NeoBundle 'https://github.com/Shougo/neocomplcache.vim.git'
endif
NeoBundle 'vcscommand.vim'
NeoBundle 'scrooloose/nerdcommenter'
NeoBundle 'vim-scripts/taglist.vim'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'kana/vim-fakeclip'
NeoBundle 'Lokaltog/vim-easymotion'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'vim-ruby/vim-ruby'

""NeoBundle 'hallison/vim-markdown'
""NeoBundle 'bronson/vim-trailing-whitespace'
""NeoBundle 'itchyny/lightline.vim'
""NeoBundle 'h1mesuke/unite-outline'
""NeoBundle 'Align'
""NeoBundle 'https://github.com/Shougo/quicrun.vim.git'
"
"--- Markdown github
NeoBundle 'kannokanno/previm'
NeoBundle 'thinca/vim-quickrun'
NeoBundle 'tyru/open-browser.vim'
NeoBundle 'superbrothers/vim-quickrun-markdown-gfm'

NeoBundle 'scrooloose/syntastic'
NeoBundle 'tpope/vim-endwise'
NeoBundle 'thinca/vim-ref'
NeoBundle 'yuku-t/vim-ref-ri'
call neobundle#end()
filetype plugin on
filetype indent on
NeoBundleCheck
"""""""""""""""""""""""""""""""""""""""""""""""
" End [ NeoBundle ]
"
