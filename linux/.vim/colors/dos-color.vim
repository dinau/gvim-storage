" Vim color file
" Maintainer:	audin <http://avr.paslog.jp>
" Last Change:	2012/06

" Black base background color, old DOS console like.


"set bg=dark
hi clear
if exists("syntax_on")
	syntax reset
endif

let colors_name = "dos-color"
" http://afsoft.jp/cad/cad/005.html

hi link Conditional Type
hi link Repeat      Type
hi link Function    Normal

hi Type         ctermfg=green guifg=lime gui=none cterm=none
hi Statement    ctermfg=green guifg=lime gui=none cterm=none
hi String       ctermfg=red   guifg=red  gui=none cterm=none

hi c_cyan		ctermfg=cyan    guifg=cyan
hi c_yellow		ctermfg=yellow  guifg=yellow
hi c_green      ctermfg=green   guifg=green
hi cb_green     ctermfg=green   guifg=green cterm=bold gui=bold
hi c_white      ctermfg=white   guifg=white
hi c_red        ctermfg=red     guifg=red
hi c_magenta    ctermfg=magenta guifg=magenta cterm=none gui=none
hi cb_magenta   ctermfg=magenta guifg=magenta cterm=bold gui=bold

hi Comment		guifg=cyan ctermfg=cyan cterm=none
hi Number		guifg=Yellow ctermfg=yellow cterm=none
hi Normal		guifg=white guibg=black		ctermfg=white ctermbg=black
hi ErrorMsg		guifg=white guibg=#287eff	ctermfg=white ctermbg=lightblue
hi Visual		guifg=#8080ff guibg=fg		gui=reverse				ctermfg=lightblue ctermbg=fg cterm=reverse
hi VisualNOS	guifg=#8080ff guibg=fg		gui=reverse,underline	ctermfg=lightblue ctermbg=fg cterm=reverse,underline
hi Todo			guifg=#d14a14 guibg=#1248d1						ctermfg=red	ctermbg=darkblue
hi Search		guifg=#90fff0 guibg=#2050d0	ctermfg=white ctermbg=darkgray cterm=none
hi IncSearch	guifg=#b0ffff guibg=#2050d0							ctermfg=darkblue ctermbg=gray

hi SpecialKey		guifg=white			ctermfg=darkcyan
hi Directory		guifg=cyan			ctermfg=cyan
hi Title			guifg=magenta gui=none ctermfg=magenta cterm=none
hi WarningMsg		guifg=red			ctermfg=red
hi WildMenu			guifg=yellow guibg=black ctermfg=yellow ctermbg=black cterm=none term=none
hi ModeMsg			guifg=#22cce2		ctermfg=lightblue
hi MoreMsg			ctermfg=darkgreen	ctermfg=darkgreen
hi Question			guifg=green gui=none ctermfg=green cterm=none
hi NonText			guifg=darkblue		ctermfg=darkblue

hi StatusLine   guifg=black guibg=darkgray gui=none ctermfg=black ctermbg=gray term=none cterm=none
hi StatusLineNC	guifg=white guibg=#444444  gui=none		ctermfg=black ctermbg=gray  term=none cterm=none
hi VertSplit	guifg=black guibg=darkgray gui=none		ctermfg=black ctermbg=gray term=none cterm=none

hi Folded	guifg=#808080 guibg=#000040			ctermfg=darkgrey ctermbg=black cterm=bold term=bold
hi FoldColumn	guifg=#808080 guibg=#000040			ctermfg=darkgrey ctermbg=black cterm=bold term=bold
hi LineNr		guifg=white	ctermfg=white cterm=none

hi Cursor	guifg=black guibg=white ctermfg=black ctermbg=white
hi lCursor	guifg=black guibg=white ctermfg=black ctermbg=white

hi Constant     guifg=lightred ctermfg=lightred cterm=none
hi Special      guifg=Orange   ctermfg=brown    cterm=none gui=none
hi Identifier   guifg=#40ffff  ctermfg=cyan    cterm=none
hi PreProc      guifg=#ff80ff  ctermfg=magenta  cterm=none gui=none
hi Underlined	cterm=underline term=underline
hi Ignore	guifg=bg ctermfg=white

" suggested by tigmoid, 2008 Jul 18
hi Pmenu guifg=#c0c0c0 guibg=#404080
hi PmenuSel guifg=#c0c0c0 guibg=#2050d0
hi PmenuSbar guifg=blue guibg=darkgray
hi PmenuThumb guifg=#c0c0c0

"
hi SpecialKey	guifg=#606060
hi Folded		guifg=#808080 guibg=#202038
hi FoldColumn	guifg=#808080 guibg=#202038
hi Pmenu        guibg=#606060   ctermbg=8
hi PmenuSel     guibg=SlateBlue ctermbg=lightblue
hi PmenuSbar    guibg=#404040   ctermbg=0
hi PmenuThumb   guifg=#606060   ctermfg=8

"対応する( )のハイライト色
hi MatchParen	guifg=white guibg=darkgray ctermfg=white ctermbg=darkgrey
"
hi SignColumn	guibg=#101020
hi CursorIM		guifg=NONE guibg=Red ctermfg=none ctermbg=red
hi CursorLine   guifg=NONE    guibg=bg  gui=underline ctermbg=8 ctermfg=NONE cterm=none
hi ZenkakuSpace guifg=#505050 gui=underline cterm=underline ctermfg=darkgrey
"hi MyBrackets   guifg=#e09e9e

hi link vimComment Comment

"Jal language
hi link picComment		Comment
hi link jalNumber 		Number
hi link jalHexNumber	Number
hi link jalBinNumber 	Number
hi link jalOperator 	Normal
hi link jalFunction		Type
hi link jalStatement 	Type
hi link jalConditional 	Type
hi link jalRepeat 		Type

" C language
hi link cString				String
hi link cCppString			cString
hi link cCharacter			cString
hi link cCommentSkip Comment
hi link cCommentString Comment
hi link cComment2String Comment
hi link cCommentL Comment
hi link cCommentStart		Comment
hi link cComment			Comment
hi link cPreProc		PreProc
hi link cInclude cPreProc
hi link cIncluded			Normal
" hi link cPreCondit cPreProc
hi link cDefine cInclude
"hi link cStatement Type
hi link cRepeat				Type
hi link cConditional Type
"hi link cOperator			Type
"hi link Statement			Type
hi link cLabel Type

" Python language
hi pythonTodo               guifg=Orange
hi link pythonStatement		Type
hi link pythonString String
hi link pythonRepeat		Type
hi link pythonConditional Type
hi link pythonBuiltin Type
hi link pythonOperator Type

" Pascal  language
hi link pascalStatement		Type
hi link pascalConditional Type
hi link pascalRepeat		Type
hi link pascalOperator Type
hi link pascalBuiltin Type
hi link pascalFunction		Type
hi link pascalString        cString

" html
hi htmlTag guifg=magenta
hi link htmlTagName Type
hi link htmlValue htmlTag
hi link htmlArg Comment
hi link htmlLink c_yellow

" vimdiff
hi DiffAdd      guibg=gray	    guifg=black ctermbg=gray term=none cterm=none
hi DiffChange   guibg=gray      guifg=black ctermbg=magenta            cterm=none
hi DiffDelete	guifg=magenta   guifg=black ctermfg=magenta ctermbg=magenta gui=bold
hi DiffText     guibg=brown     ctermbg=yellow   ctermfg=black  cterm=none gui=none

" diff
hi link diffAdded	Type
hi link diffRemoved	c_magenta
hi link diffFile	Number
hi link diffChanged	Normal
hi link diffOldFile	Normal
hi link diffSubname Normal
hi link diffLine    c_magenta

" Lua language
hi link luaFunc Type

"folded color
hi link Folded    c_yellow

" Makefile
hi link makeIdent           c_white
hi link makeSpecTarget c_yellow
hi link makeOverride wildMenu
hi link makeTarget	c_yellow
hi link makeCommands wildMenu
hi link makeSource wildMenu
hi link makeImplicit c_cyan
hi link makeBString c_cyan
hi link makeCommandSync c_cyan

" Rust language
hi link rustKeyword         Type

" JSon
hi link jsonBraces          comment
hi link jsonString          comment
hi link jsonComment         comment
hi link jsonCommentError    c_magenta

" vim-gitgutter 1
" GitGutterAdd          " an added line
" GitGutterChange       " a changed line
" GitGutterDelete       " at least one removed line
" GitGutterChangeDelete " a changed line followed by at least one removed line
hi GitGutterAdd             guifg=green     ctermfg=green
hi GitGutterChange          guifg=cyan      ctermfg=cyan
hi GitGutterDelete          guifg=magenta   ctermfg=magenta
hi GitGutterChangeDelete    guifg=magenta   ctermfg=magenta

" vim-gitgutter 2
" GitGutterAddLine          " default: links to DiffAdd
" GitGutterChangeLine       " default: links to DiffChange
" GitGutterDeleteLine       " default: links to DiffDelete
" GitGutterChangeDeleteLine " default: links to GitGutterChangeLineDefault, i.e. DiffChange

