copy /y ..\.vimrc win\
copy /y ..\.gvimrc win\
copy /y ..\.spacemacs emacs\
copy /y ..\vimfiles\syntax               win\vimfiles\syntax
copy /y ..\vimfiles\indent               win\vimfiles\indent
copy /y ..\vimfiles\colors\dos-color.vim win\vimfiles\colors\
copy /y ..\.gitignore  git\
copy /y ..\.gitconfig  git\
nkf32 -Sw win\.vimrc > win\.vimrc.utf8
pause

