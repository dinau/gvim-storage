" .gvimrc file
" Maintainer:	audin <http://avr.paslog.jp>
" Last Change:	2012/06

" このファイル自身のエンコードは「shift-jis」、改行コードはCR+LF。
" Linuxで使うときは「utf-8」に、改行コードはLFにコンバートが必要。

" Font setup
"set guifontwide="源ノ角ゴシック Code JP"
"set guifont=Source_Code_Pro_Light:h14:cANSI
"set guifont=MS_Gothic:h12
"set guifont=Ricty_Diminished:h14
set guifont=Myrica_M:h14
"set guifontwide=
"set guifontwide=Inconsolata:h14,Consolas:h10,Lucida_Console:h10:w5,MS_Gothic:h12:cSHIFTJIS
"set guifontwide=MS_Gothic:h12:cSHIFTJIS
"set guifont=メイリオ:h12:cSHIFTJIS




" マルチバイト文字系のlistcharsで表示される設定はgvimrcに書かないと反映されない
"set listchars=precedes:<,extends:>,eol:?,tab:>\
"set listchars=tab:>-,trail:-,eol:$,extends:>,precedes:<,nbsp:%


" カラー設定: ファイル名を指定する
colorscheme dos-color

" ウインドウサイズ
let g:save_window_file = expand('~/_vimwinpos')
augroup SaveWindow
  autocmd!
  autocmd VimLeavePre * call s:save_window()
  function! s:save_window()
    let options = [
      \ 'set columns=' . &columns,
      \ 'set lines=' . &lines,
      \ 'winpos ' . getwinposx() . ' ' . getwinposy(),
      \ ]
    call writefile(options, g:save_window_file)
  endfunction
augroup END

if filereadable(g:save_window_file)
  execute 'source' g:save_window_file
endif

"http://sites.google.com/site/fudist/Home/vim-nihongo-ban/tips#TOC-9
".gvimrcに以下を付け加えると強制保存と、更新時のみ保存をメニューでも使い分けられます。
silent! aunmenu &File.保存(&S)
amenu <silent> 10.340 &File.保存(&W)<Tab>:w  :if expand('%') == ''<Bar>browse confirm w<Bar>else<Bar>confirm w<Bar>endif<CR>
amenu <silent> 10.341 &File.更新時保存(&S)<Tab>:update  :if expand('%') == ''<Bar>browse confirm w<Bar>else<Bar>confirm update<Bar>endif<CR>



